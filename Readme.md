## SDG Games base for Unity games
This project is a collection of free assets I found and libraries I wrote combined to form a single Unity project. I may have modified some of the assets from their original form. This is the starting point for all SDGGames projects. If you find this project useful, please check out the original authors for the plugins I used.

### Credits/Licenses
The following plugins/tools/assets were used in the creation of this project:

Gitlab Ci/Cd was based on https://gitlab.com/gableroux/unity3d-gitlab-ci-example and uses
gabelroux's Unity docker images (https://hub.docker.com/r/gableroux/unity3d/)

The MoonSharp plugin at Assets/Plugins/MoonSharp is from the MoonSharp project (https://github.com/moonsharp-devs/moonsharp)
Copyright (c) 2014-2016, Marco Mastropaolo - released under BSG license
See https://github.com/moonsharp-devs/moonsharp/blob/master/LICENSE for more details

The Command Terminal plugin at Assets/Plugins/CommandTerminal is from the Command Terminal project (https://github.com/stillwwater/command_terminal):
Copyright (c) 2018 Lucas - released under the MIT license

The Moments Recorder plugin at Assets/Plugins/MomentsGifRecorder is from the Moments Recorder project (https://github.com/Chman/Moments)
Copyright (c) 2015 Thomas Hourdel - released under the zLib license

See the LICENSE file for the full license.