﻿using UnityEngine;

namespace Debugging
{
    internal class CaptureMenu : DebugMenuPage
    {
        /// <summary>
        /// Defines a list of Terminal Commands relevant to this menu. Can be called from the terminal interface or through a menu interaction.
        /// </summary>
        internal static class TerminalCommands
        {
#pragma warning disable IDE0060 // Remove unused parameterusing CommandTerminal;

#pragma warning restore IDE0060 // Remove unused parameterusing CommandTerminal;
        }

        private ScreenshotManager screenshotManager;

        public CaptureMenu()
        {
            this.screenshotManager = Object.FindObjectOfType<ScreenshotManager>();
        }

        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public override string Name => "Capture & Record";

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// </summary>
        public override void Draw()
        {
            // Two side-by-side menus.
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            
            GUILayout.Label("Press the G key to save a gif of the last " + this.screenshotManager.RecordingTime + " seconds.");

            GUILayout.BeginHorizontal();
            GUILayout.Label("Width:                   " + this.screenshotManager.GifWidth);
            int width = (int)GUILayout.HorizontalSlider(this.screenshotManager.GifWidth, 8, Screen.width, GUILayout.MinWidth(200));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            bool autoAspect = GUILayout.Toggle(this.screenshotManager.AutoAspect, "Automatic Window Height: ");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            int height = this.screenshotManager.GifHeight;
            if (autoAspect && Camera.main != null)
            {
                height = Mathf.RoundToInt(width / Camera.main.aspect);
                GUILayout.Label("Window Height: " + height);
            }
            else
            {
                GUILayout.Label("Height:                  " + height);
                height = (int)GUILayout.HorizontalSlider(this.screenshotManager.GifHeight, 8, Screen.height, GUILayout.MinWidth(200));
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Frames Per Second:       " + this.screenshotManager.FramePerSecond);
            int framesPerSecond = (int)GUILayout.HorizontalSlider(this.screenshotManager.FramePerSecond, 1, 30, GUILayout.MinWidth(200));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("RepeatCount:             " + this.screenshotManager.Repeat);
            int repeat = (int)GUILayout.HorizontalSlider(this.screenshotManager.Repeat, -1, 100, GUILayout.MinWidth(200));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Quality:                 " + this.screenshotManager.Quality);
            int quality = (int)GUILayout.HorizontalSlider(this.screenshotManager.Quality, 1, 100, GUILayout.MinWidth(200));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Recoding Time (Seconds): " + this.screenshotManager.RecordingTime);
            float recordingTime = (int)GUILayout.HorizontalSlider(this.screenshotManager.RecordingTime, 0.1f, 120, GUILayout.MinWidth(200));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            // Update settings if they changed.
            if (this.screenshotManager.GifWidth != width)
                this.screenshotManager.GifWidth = width;
            if (this.screenshotManager.GifHeight != height)
                this.screenshotManager.GifHeight = height;
            if (this.screenshotManager.AutoAspect != autoAspect)
                this.screenshotManager.AutoAspect = autoAspect;
            if (this.screenshotManager.FramePerSecond != framesPerSecond)
                this.screenshotManager.FramePerSecond = framesPerSecond;
            if (this.screenshotManager.Repeat != repeat)
                this.screenshotManager.Repeat = repeat;
            if (this.screenshotManager.Quality != quality)
                this.screenshotManager.Quality = quality;
            if (this.screenshotManager.RecordingTime != recordingTime)
                this.screenshotManager.RecordingTime = recordingTime;

            // APPLY button
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Apply Settings"))
            {
                this.screenshotManager.ApplySettings();
            }
            GUILayout.EndHorizontal();

            // Toggle active recording ON OFF

            // STATISTICS OUTPUT:
            GUILayout.Label("Press [G] to export the buffered frames to a gif file.");
            GUILayout.Label("Recorder State : " + this.screenshotManager.Recorder.State.ToString());

            if (this.screenshotManager.IsSaving)
                GUILayout.Label("Progress Report : " + this.screenshotManager.Progress.ToString("F2") + "%");

            if (!string.IsNullOrEmpty(this.screenshotManager.LastGifFile))
                GUILayout.Label("Last File Saved : " + this.screenshotManager.LastGifFile);


            // End split submenu.
            GUILayout.EndVertical();

            GUILayout.BeginVertical();

            GUILayout.Label("Press [P] to take a screenshot and save it to file.");
            // TODO: Remappable Key

            // RESOLUTION SELECT (1/4. 1/2, 1, 2, 4, 8) (maybe select based on current res)

            // STATISTICS OUTPUT: Last screenshot name and location

            // End menu split.
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
    }
}
