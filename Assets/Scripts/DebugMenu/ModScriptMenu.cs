﻿using CommandTerminal;
using Callouts;
using UnityEngine;
using Interface;

namespace Debugging
{
    internal class ModScriptMenu : DebugMenuPage
    {
        /// <summary>
        /// Defines a list of Terminal Commands relevant to this menu. Can be called from the terminal interface or through a menu interaction.
        /// </summary>
        internal static class TerminalCommands
        {
#pragma warning disable IDE0060 // Remove unused parameterusing CommandTerminal;
            [RegisterCommand(Name = "HelloWorld", Help = "Calls the HelloWorld function in the mod scripts. (Helps verify that the mods are working as expected on your machine).", MinArgCount = 0, MaxArgCount = 1)]
            private static void HelloWorld(CommandArg[] args)
            {
                Mod_HelloWorld.SayHello();
            }
            [RegisterCommand(Name = "ReloadModScripts", Help = "Reloads the Lua mod scripts. Active Ais will use the old scripts until they are destroyed.", MinArgCount = 0, MaxArgCount = 0)]
            private static void ReloadScripts(CommandArg[] args) => ModManager.RefreshCache();

            [RegisterCommand(Name = "Lua", Help = "Attempts to run the entered text as a Lua command", MinArgCount = 1)]
            private static void RunLuaCommand(CommandArg[] args) => ModScriptRunner.Call(string.Join(" ", args));


            [RegisterCommand(Name = "RunModTest", Help = "Runs the unit test suite for the active version of the specified mod", MinArgCount = 1)]
            private static void RunModTest(CommandArg[] args) => ModScriptRunner.Call(args[0].String + "Tests");
#pragma warning restore IDE0060 // Remove unused parameterusing CommandTerminal;
        }

        private bool showModScriptSettings;
        private bool showModScriptDirectoryTree;
        private bool showModScriptFileContents;
        private bool showAiScriptSettings;
        private bool showAiScriptDirectoryTree;
        private bool showAiScriptFileContents;

        public ModScriptMenu()
        {
            this.showModScriptSettings = true;
            this.showModScriptDirectoryTree = true;
            this.showModScriptFileContents = false;
            this.showAiScriptSettings = true;
            this.showAiScriptDirectoryTree = true;
            this.showAiScriptFileContents = false;
        }

        /// <summary>
        /// The name of the menu, will be displayed in a list of menu tabs before the menu is selected.
        /// </summary>
        public override string Name => "Mod and AI Scripts";

        /// <summary>
        /// Draws the menu buttons. Each menu page will define this on its own.
        /// </summary>
        public override void Draw()
        {
            GUILayout.Label("Mod and Ai script management menu.");
            GUILayout.Space(16);
            if (this.showModScriptSettings)
            {
                if (GUILayout.Button("Hide Mod Script Management Menu"))
                    this.showModScriptSettings = false;

                this.DrawModScriptSettingsMenu();
            }
            else
            {
                if (GUILayout.Button("Show Mod Script Management Menu"))
                    this.showModScriptSettings = true;
            }

            GUILayout.Space(16);

            if (this.showAiScriptSettings)
            {
                if (GUILayout.Button("Hide Ai Script Management Menu"))
                    this.showAiScriptSettings = false;

                this.DrawAiScriptSettingsMenu();
            }
            else
            {
                if (GUILayout.Button("Show Ai Script Management Menu"))
                    this.showAiScriptSettings = true;
            }
        }

        /// <summary>
        /// Draw the mod script submenu.
        /// </summary>
        private void DrawModScriptSettingsMenu()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reload all Scripts and Apply Changes"))
                this.RunCommand("ReloadModScripts");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(16);

            ScriptCache modScriptCache = ModManager.ModScripts;
            if (modScriptCache != null)
            {
                GUILayout.Label("Mod Scripts:");
                foreach (string modName in modScriptCache.manafest.subDirectories)
                {
                    // Draw a toggle for the top-level folder and see if we need to toggle it.
                    FileManafest modFolder = modScriptCache.manafest.subDirectoryContents[modName];
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Toggle(modFolder.IsDirectoryActive, modName) != modFolder.IsDirectoryActive)
                    {
                        // Use the interface in case we need to do anything besides setting the flag.
                        ModManager.SetModActive(modName, !modFolder.IsDirectoryActive);
                        this.RunCommand("ReloadModScripts");
                    }
                    // Run unit tests here.
                    if (modFolder.IsDirectoryActive)
                        if (GUILayout.Button("Run Unit Tests"))
                            this.RunCommand("RunModTest " + modName);
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    if (modFolder.IsDirectoryActive)
                    {
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(16);
                        GUILayout.BeginVertical();
                        foreach (string modVariantName in modFolder.subDirectories)
                        {
                            // Skip the tests folder, this one shouldn't be turned off.
                            if (modVariantName != "Tests")
                            {
                                FileManafest modVariant = modFolder.subDirectoryContents[modVariantName];

                                // Don't allow un-selection, just show the active variant and draw contents.
                                if (modVariant.IsDirectoryActive)
                                {
                                    _ = GUILayout.Toggle(true, modVariantName);
                                    this.DrawAllFoldersAndFiles(modVariant, 2);
                                }
                                else if (GUILayout.Toggle(false, modVariantName))
                                {
                                    ModManager.SelectModVariant(modName, modVariantName);
                                    this.RunCommand("ReloadModScripts");
                                }
                            }
                        }
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();
                    }
                }
            }

            // Draw a list of all loaded mod files with full paths.
            if (modScriptCache != null)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Loaded Mod Script Files:");
                if (this.showModScriptDirectoryTree)
                {
                    if (GUILayout.Button("Hide"))
                        this.showModScriptDirectoryTree = false;

                    if (this.showModScriptFileContents)
                    {
                        if (GUILayout.Button("Hide File Contents"))
                            this.showModScriptFileContents = false;
                    }
                    else if (GUILayout.Button("Show File Contents"))
                        this.showModScriptFileContents = true;
                }
                else if (GUILayout.Button("Show"))
                    this.showModScriptDirectoryTree = true;

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                if (this.showModScriptDirectoryTree)
                {
                    string fileNames = "";
                    foreach (string name in modScriptCache.fileContents.Keys)
                    {
                        fileNames += "    -- " + name + '\n';
                        if (this.showModScriptFileContents)
                            fileNames += modScriptCache.fileContents[name] + "\n\n";
                    }
                    GUILayout.Label(fileNames);
                }
            }
        }

        /// <summary>
        /// Draw the ai script submenu.
        /// </summary>
        private void DrawAiScriptSettingsMenu()
        {
            ScriptCache aiScriptCache = ModManager.AiScripts;

            if (aiScriptCache != null)
            {
                GUILayout.Label("Ai Scripts:");
                foreach (string aiName in aiScriptCache.manafest.subDirectories)
                {
                    // Draw a toggle for the top-level folder and see if we need to toggle it.
                    FileManafest aiFolder = aiScriptCache.manafest.subDirectoryContents[aiName];
                    if (GUILayout.Toggle(aiFolder.IsDirectoryActive, aiName) != aiFolder.IsDirectoryActive)
                    {
                        // Use the interface in case we need to do anything besides setting the flag.
                        ModManager.SetAiActive(aiName, !aiFolder.IsDirectoryActive);
                        this.RunCommand("ReloadModScripts");
                    }

                    // Draw all subdirectories, we don't need to be that granular.
                    if (aiFolder.IsDirectoryActive)
                        this.DrawAllFoldersAndFiles(aiFolder, 1);
                }
            }

            // Draw a list of all loaded ai files with full paths.
            if (aiScriptCache != null)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Loaded Ai Script Files:");
                if (this.showAiScriptDirectoryTree)
                {
                    if (GUILayout.Button("Hide"))
                        this.showAiScriptDirectoryTree = false;

                    if (this.showAiScriptFileContents)
                    {
                        if (GUILayout.Button("Hide File Contents"))
                            this.showAiScriptFileContents = false;
                    }
                    else if (GUILayout.Button("Show File Contents"))
                        this.showAiScriptFileContents = true;
                }
                else if (GUILayout.Button("Show"))
                    this.showAiScriptDirectoryTree = true;

                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                if (this.showAiScriptDirectoryTree)
                {
                    string fileNames = "";
                    foreach (string name in aiScriptCache.fileContents.Keys)
                    {
                        fileNames += "    -- " + name + '\n';
                        if (this.showModScriptFileContents)
                            fileNames += aiScriptCache.fileContents[name] + "\n\n";
                    }
                    GUILayout.Label(fileNames);
                }
            }
        }

        private void DrawAllFoldersAndFiles(FileManafest manafest, int depth)
        {
            // Draw all files.
            foreach (string fileName in manafest.files)
            {
                GUILayout.Space(-10);
                GUILayout.BeginHorizontal();
                GUILayout.Space(depth * 16);

                GUILayout.Label("- " + fileName);
                GUILayout.EndHorizontal();
            }
            // Draw all folders and recurse into them.
            foreach (string dirName in manafest.subDirectories)
            {
                FileManafest subdir = manafest.subDirectoryContents[dirName];

                GUILayout.Space(-10);
                GUILayout.BeginHorizontal();
                GUILayout.Space(depth * 16);

                GUILayout.Label(dirName);
                GUILayout.EndHorizontal();

                this.DrawAllFoldersAndFiles(subdir, depth + 1);
            }
        }
    }
}
