﻿using UnityEngine;
using UnityEngine.UI;

/* TODO:
 * X I. Create a basic game
 *  X 1. Create and render a basic board.
 *  X 2. Enable board states to change when clicked.
 *  X 3. Add simple rule checks (basic game in C#)
 *  X 4. Add win/lose detection and win count/new game.
 * 
 * II. Add AI player
 *    1. Connect AI scripts to project.
 *    2. Expose data structure to AI.
 *    3. Enable AI to send inputs.
 *  
 * III. Initialize modscripts
 *    1. Create script to set up a new game.
 *    2. Create script to check for victory.
 *    3. Create script to determine if an input is valid.
 */

public class GameManager : MonoBehaviour
{

    public Text[] spaceList;
    public GameObject gameOverPanel;
    public Text gameOverText;
    public GameObject restartButton;
    private string side;
    private int moves;

    // Start is called before the first frame update
    private void Start()
    {
        this.SetGameControllerReferenceForButtons();
        this.side = "X";
        this.gameOverPanel.SetActive(false);
        this.moves = 0;
        this.restartButton.SetActive(false);
    }

    private void SetGameControllerReferenceForButtons()
    {
        for (int i = 0; i < this.spaceList.Length; i++)
            this.spaceList[i].GetComponentInParent<Space>().SetControllerReference(this);
    }

    public string GetSide()
    {
        return this.side;
    }

    private void ChangeSide()
    {
        if (this.side == "X")
            this.side = "O";
        else
            this.side = "X";
    }

    public void EndTurn()
    {
        this.moves++;
        if (this.spaceList[0].text == this.side && this.spaceList[1].text == this.side && this.spaceList[2].text == this.side)
            this.GameOver();
        else if (this.spaceList[3].text == this.side && this.spaceList[4].text == this.side && this.spaceList[5].text == this.side)
            this.GameOver();
        else if (this.spaceList[6].text == this.side && this.spaceList[7].text == this.side && this.spaceList[8].text == this.side)
            this.GameOver();
        else if (this.spaceList[0].text == this.side && this.spaceList[3].text == this.side && this.spaceList[6].text == this.side)
            this.GameOver();
        else if (this.spaceList[1].text == this.side && this.spaceList[4].text == this.side && this.spaceList[7].text == this.side)
            this.GameOver();
        else if (this.spaceList[2].text == this.side && this.spaceList[5].text == this.side && this.spaceList[8].text == this.side)
            this.GameOver();
        else if (this.spaceList[0].text == this.side && this.spaceList[4].text == this.side && this.spaceList[8].text == this.side)
            this.GameOver();
        else if (this.spaceList[2].text == this.side && this.spaceList[4].text == this.side && this.spaceList[6].text == this.side)
            this.GameOver();
        else if (this.moves >= 9)
        {
            this.gameOverPanel.SetActive(true);
            this.gameOverText.text = "Tie!";
            this.restartButton.SetActive(true);
        }
        this.ChangeSide();
    }

    private void GameOver()
    {
        this.gameOverPanel.SetActive(true);
        this.gameOverText.text = this.side + " wins!";
        this.restartButton.SetActive(true);
        for (int i = 0; i < this.spaceList.Length; i++)
            this.SetInteractable(false);
    }

    private void SetInteractable(bool setting)
    {
        for (int i = 0; i < this.spaceList.Length; i++)
            this.spaceList[i].GetComponentInParent<Button>().interactable = setting;
    }

    public void Restart()
    {
        this.side = "X";
        this.moves = 0;
        this.gameOverPanel.SetActive(false);
        this.SetInteractable(true);
        this.restartButton.SetActive(false);
        for (int i = 0; i < this.spaceList.Length; i++)
            this.spaceList[i].text = "";
    }
}
