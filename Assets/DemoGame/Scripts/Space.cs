﻿using UnityEngine;
using UnityEngine.UI;

public class Space : MonoBehaviour
{
    public Button button;
    public Text buttonText;

    private GameManager gameManager;

    public void SetControllerReference(GameManager manager)
    {
        this.gameManager = manager;
    }

    public void SetSpace()
    {
        this.buttonText.text = this.gameManager.GetSide();
        this.button.interactable = false;
        this.gameManager.EndTurn();
    }
}
