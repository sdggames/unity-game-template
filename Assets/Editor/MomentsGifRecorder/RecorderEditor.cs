﻿/*
 * Copyright (c) 2015 Thomas Hourdel
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 *    1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 
 *    2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 
 *    3. This notice may not be removed or altered from any source
 *    distribution.
 */

using UnityEngine;
using UnityEditor;
using Moments;

namespace MomentsEditor
{
	[CustomEditor(typeof(Recorder))]
	public sealed class RecorderEditor : Editor
	{
		SerializedProperty m_AutoAspect;
		SerializedProperty m_Width;
		SerializedProperty m_Height;
		SerializedProperty m_FramePerSecond;
		SerializedProperty m_Repeat;
		SerializedProperty m_Quality;
		SerializedProperty m_BufferSize;
		SerializedProperty m_WorkerPriority;

		void OnEnable()
		{
            this.m_AutoAspect = this.serializedObject.FindProperty("m_AutoAspect");
            this.m_Width = this.serializedObject.FindProperty("m_Width");
            this.m_Height = this.serializedObject.FindProperty("m_Height");
            this.m_FramePerSecond = this.serializedObject.FindProperty("m_FramePerSecond");
            this.m_Repeat = this.serializedObject.FindProperty("m_Repeat");
            this.m_Quality = this.serializedObject.FindProperty("m_Quality");
            this.m_BufferSize = this.serializedObject.FindProperty("m_BufferSize");
            this.m_WorkerPriority = this.serializedObject.FindProperty("WorkerPriority");
		}

		public override void OnInspectorGUI()
		{
			Recorder recorder = (Recorder)this.target;

			EditorGUILayout.HelpBox("This inspector is only used to tweak default values for the component. To change values at runtime, use the Setup() method.", MessageType.Info);

			// Don't let the user tweak settings while playing as it may break everything
			if (Application.isEditor && Application.isPlaying)
				GUI.enabled = false;

            this.serializedObject.Update();

			// Hooray for propertie drawers !
			EditorGUILayout.PropertyField(this.m_AutoAspect, new GUIContent("Automatic Height", "Automatically compute height from the current aspect ratio."));
			EditorGUILayout.PropertyField(this.m_Width, new GUIContent("Width", "Output gif width in pixels."));

			if (!this.m_AutoAspect.boolValue)
				EditorGUILayout.PropertyField(this.m_Height, new GUIContent("Height", "Output gif height in pixels."));
			else
				EditorGUILayout.LabelField(new GUIContent("Height", "Output gif height in pixels."), new GUIContent(this.m_Height.intValue.ToString()));

			EditorGUILayout.PropertyField(this.m_WorkerPriority, new GUIContent("Worker Thread Priority", "Thread priority to use when processing frames to a gif file."));
			EditorGUILayout.PropertyField(this.m_Quality, new GUIContent("Compression Quality", "Lower values mean better quality but slightly longer processing time. 15 is generally a good middleground value."));
			EditorGUILayout.PropertyField(this.m_Repeat, new GUIContent("Repeat", "-1 to disable, 0 to loop indefinitely, >0 to loop a set number of time."));
			EditorGUILayout.PropertyField(this.m_FramePerSecond, new GUIContent("Frames Per Second", "The number of frames per second the gif will run at."));
			EditorGUILayout.PropertyField(this.m_BufferSize, new GUIContent("Record Time", "The amount of time (in seconds) to record to memory."));

            this.serializedObject.ApplyModifiedProperties();

			GUI.enabled = true;

			recorder.ComputeHeight();
			EditorGUILayout.LabelField("Estimated VRam Usage", recorder.EstimatedMemoryUse.ToString("F3") + " MB");
		}
	}
}
