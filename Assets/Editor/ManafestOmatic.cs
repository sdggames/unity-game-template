﻿using System.IO;

/// <summary>
/// "Build your Manifest files autOmagically" TM
/// I was up way too late when I wrote this, so I decided to go with magical name!
/// The ManafestOmatic will take a directory and label every file so you can parse it
/// even in WebGL! Yay! 
/// </summary>
public static class ManafestOmatic
{
    /// <summary>
    /// Creates a bunch of .manafest* files containing a .json object (FileManafest, to be precise)
    /// so we can parse directories and load files even when filesystem operations don't work!
    /// (I'm looking at you, WebGl!) -_-
    /// 
    /// * Not to be confused with the always popular .manifest files. Mana is a video game convention. Thus, *manafest*
    /// </summary>
    /// <param name="targetDirectory"></param>
    public static void ProcessDirectory(string targetDirectory)
    {
        FileManafest manafest = new FileManafest();
        manafest.BuildManafestsRecursively(targetDirectory);
        SaveManafests(manafest, targetDirectory);
    }

/// <summary>
/// The unloved function that does the *actual* work while ProcessDirectory gets all of the glory...
/// </summary>
/// <param name="manafest"></param>
private static void SaveManafests(FileManafest manafest, string targetDirectory)
    {
        // Use the File manager to save the manifest as a Json file so we 
        // can use it later to help the fileManager manage files better!
        string manafestPath = Path.Combine(targetDirectory, "manafest");

        FileManager.SaveFileAbsolute(manafestPath, manafest);

        // Do it again and again and again and again...
        foreach (string directory in manafest.subDirectories)
            SaveManafests(manafest.subDirectoryContents[directory], Path.Combine(targetDirectory, directory));
    }
}
