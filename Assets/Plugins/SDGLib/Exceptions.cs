﻿using System;

public class SaveFileException : Exception
{
    public SaveFileException(string s) : base(s) { }
}

public class LoadFileException : Exception
{
    public LoadFileException(string s) : base(s) { }
}

public class OutOfBoundsException : Exception
{
    public OutOfBoundsException(string s) : base(s) { }
}

public class ObjectNotFoundException : Exception
{
    public ObjectNotFoundException(string s) : base(s) { }
}

public class SingletonReInstantiatedException : Exception
{
    public SingletonReInstantiatedException(string s) : base(s) { }
}

public class SingletonNotInstantiatedException : Exception
{
    public SingletonNotInstantiatedException(string s) : base(s) { }
}

public class InvalidIndexException : Exception
{
    public InvalidIndexException(string s) : base(s) { }
}

public class InvalidSwitchCaseException : Exception
{
    public InvalidSwitchCaseException(string s) : base(s) { }
}

public class NullPrefabException : NullReferenceException
{
    public NullPrefabException(string s) : base(s) { }
}
