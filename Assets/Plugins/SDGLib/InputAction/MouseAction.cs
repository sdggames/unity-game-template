﻿using System;
using System.Collections.Generic;

/// <summary>
/// Do something when a mouse click is registered at a location.
/// </summary>
/// <param name="mouseDownLocation"></param>
public delegate void ProcessClick(UnityEngine.Vector2 mouseDownLocation);

/// <summary>
/// Do something when a drag event is registered at a location.
/// </summary>
/// <param name="mouseDownLocation"></param>
/// <param name="mouseUpLocation"></param>
public delegate void ProcessDrag(UnityEngine.Vector2 mouseDownLocation, UnityEngine.Vector2 mouseUpLocation);

/// <summary>
/// Maps a Mouse event to an Action. Each action can call up to 3 functions:
/// - ProcessMouseTap - call when a rapid click is registered.
/// - ProcessMouseDrag - call when the mouse is currently drawing a box.
/// - ProcessMouseDragRelease - call when a drawn box is released.
/// </summary>
[Serializable]
public class MouseAction : GenericAction, IMouseClick, IEquatable<MouseAction>
{
    public ProcessClick ProcessMouseClick;
    public ProcessDrag ProcessMouseDrag;
    public ProcessDrag ProcessMouseDragRelease;

    /// <summary>
    /// Press and release the mouse in short succession. Only uses one coordinate location.
    /// </summary>
    public void OnMouseClick(UnityEngine.Vector2 mouseDownLocation) => this.ProcessMouseClick?.Invoke(mouseDownLocation);

    /// <summary>
    /// Process an ongoing mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    public void OnMouseDrag(UnityEngine.Vector2 mouseDownLocation, UnityEngine.Vector2 mouseUpLocation) => this.ProcessMouseDrag?.Invoke(mouseDownLocation, mouseUpLocation);

    /// <summary>
    /// Finishes processing a mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    public void OnMouseDragRelease(UnityEngine.Vector2 mouseDownLocation, UnityEngine.Vector2 mouseUpLocation) => this.ProcessMouseDragRelease?.Invoke(mouseDownLocation, mouseUpLocation);

    public override bool Equals(object obj) => obj is MouseAction action && this.Action == action.Action && EqualityComparer<ProcessClick>.Default.Equals(this.ProcessMouseClick, action.ProcessMouseClick) && EqualityComparer<ProcessDrag>.Default.Equals(this.ProcessMouseDrag, action.ProcessMouseDrag) && EqualityComparer<ProcessDrag>.Default.Equals(this.ProcessMouseDragRelease, action.ProcessMouseDragRelease);

    public bool Equals(MouseAction other)
    {
        return other != null &&
               this.Action == other.Action &&
               EqualityComparer<ProcessClick>.Default.Equals(this.ProcessMouseClick, other.ProcessMouseClick) &&
               EqualityComparer<ProcessDrag>.Default.Equals(this.ProcessMouseDrag, other.ProcessMouseDrag) &&
               EqualityComparer<ProcessDrag>.Default.Equals(this.ProcessMouseDragRelease, other.ProcessMouseDragRelease);
    }

    public override int GetHashCode()
    {
        int hashCode = -501404760;
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(this.Action);
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessClick>.Default.GetHashCode(this.ProcessMouseClick);
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessDrag>.Default.GetHashCode(this.ProcessMouseDrag);
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessDrag>.Default.GetHashCode(this.ProcessMouseDragRelease);
        return hashCode;
    }

    public static bool operator ==(MouseAction left, MouseAction right) => EqualityComparer<MouseAction>.Default.Equals(left, right);
    public static bool operator !=(MouseAction left, MouseAction right) => !(left == right);
}
