﻿using UnityEngine;

namespace Debugging
{
    internal class FPSCounter
    {
        internal int frameRange = 120;

        internal static int AverageFPS { get; private set; }
        internal static int HighestFPS { get; private set; }
        internal static int LowestFPS { get; private set; }

        int[] fpsBuffer;
        int fpsBufferIndex;

        internal void UpdateFps()
        {
            if (this.fpsBuffer == null || this.fpsBuffer.Length != this.frameRange)
            {
                this.InitializeBuffer();
            }
            this.UpdateBuffer();
            this.CalculateFPS();
        }

        private void InitializeBuffer()
        {
            if (this.frameRange <= 0)
            {
                this.frameRange = 1;
            }
            this.fpsBuffer = new int[this.frameRange];
            this.fpsBufferIndex = 0;
        }

        private void UpdateBuffer()
        {
            this.fpsBuffer[this.fpsBufferIndex++] = (int)(1f / Time.unscaledDeltaTime);
            if (this.fpsBufferIndex >= this.frameRange)
            {
                this.fpsBufferIndex = 0;
            }
        }

        private void CalculateFPS()
        {
            int sum = 0;
            int highest = 0;
            int lowest = int.MaxValue;
            for (int i = 0; i < this.frameRange; i++)
            {
                int fps = this.fpsBuffer[i];
                sum += fps;
                if (fps > highest)
                {
                    highest = fps;
                }
                if (fps < lowest)
                {
                    lowest = fps;
                }
            }
            AverageFPS = (int)((float)sum / this.frameRange);
            HighestFPS = highest;
            LowestFPS = lowest;
        }
    }
}
