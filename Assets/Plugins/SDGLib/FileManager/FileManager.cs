﻿using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
using UnityEngine.Networking;
#endif

/// <summary>
/// Manages all Json and disk operations. Generic class knows nothing of the data it is using.
/// Two function groups will manage data access to either the StreamingAssets folder or the persistent data path.
/// </summary>
public static class FileManager
{
    [DllImport("__Internal")]
    private static extern void SyncFiles();

    /// <summary>
    /// Callback for the FileManager.
    /// Success will be true if the file was loaded and false if an error occurs.
    /// The object is ready to use when this is called, even if success==false.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="loadedObject"></param>
    /// <param name="success"></param>
    public delegate void LoadComplete<T>(T loadedObject, bool success);
    public delegate void LoadComplete(bool success);

    /// <summary>
    /// Saves a serializable asset to the StreamingAssets folder.
    /// THIS FUNCTION DOES NOT WORK ON WEBGL!
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fileName"></param>
    /// <param name="saveData"></param>
    public static void SaveStreamingAsset<T>(string fileName, T saveData)
    {
        string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

        SaveString(JsonUtility.ToJson(saveData, true), dataPath);
    }

    /// <summary>
    /// Saves a serializable asset to the PersistentDataPath.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fileName"></param>
    /// <param name="saveData"></param>
    public static void SavePersistentData<T>(string fileName, T saveData)
    {
        string dataPath = Path.Combine(Application.persistentDataPath, fileName);

        SaveFileAbsolute(dataPath, saveData);
    }

    /// <summary>
    /// Saves a serializable asset to an absolute path (does not append anything to the path).
    /// Use at your own risk, this is meant primarily for editor use.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="absoluteFilePath"></param>
    /// <param name="saveData"></param>
    public static void SaveFileAbsolute<T>(string absoluteFilePath, T saveData)
    {
        SaveString(JsonUtility.ToJson(saveData, true), absoluteFilePath);

        if (Application.platform == RuntimePlatform.WebGLPlayer)
            SyncFiles();
    }

    /// <summary>
    /// Loads a serializable asset from the StreamingAssets folder. The LoadComplete callback
    /// will be called when the object is ready to be used. All existing fields will be
    /// overwritten.
    /// 
    /// If the file cannot be found, the asset fields will retain their current values.
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="loadData"></param>
    /// <param name="complete"></param>
    /// <returns></returns>
    public static IEnumerator LoadStreamingAsset(string fileName, object loadData, LoadComplete complete)
    {
        string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
        using (UnityWebRequest request = UnityWebRequest.Get(dataPath))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                JsonUtility.FromJsonOverwrite("", loadData);
                complete(false);
            }
            else // Success
            {
                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, loadData);
                complete(true);
            }
        }
#else
        try
        {
            JsonUtility.FromJsonOverwrite(LoadString(dataPath), loadData);
            complete(true);
        }
        catch (IOException)
        {
            JsonUtility.FromJsonOverwrite("", loadData);
            complete(false);
        }

        yield return null;
#endif
    }

    public static IEnumerator LoadStreamingAsset(string fileName, object loadData)
    {
        string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
        using (UnityWebRequest request = UnityWebRequest.Get(dataPath))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
                JsonUtility.FromJsonOverwrite("", loadData);
            else // Success
                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, loadData);
        }
#else
        try
        {
            JsonUtility.FromJsonOverwrite(LoadString(dataPath), loadData);
        }
        catch (IOException)
        {
            JsonUtility.FromJsonOverwrite("", loadData);
        }

        yield return null;
#endif
    }

    /// <summary>
    /// Loads a serializable asset from the StreamingAssets folder into a new object. The LoadComplete
    /// callback will be called when the object is ready to be used.
    /// 
    /// If the file cannot be found, an empty asset will be returned.
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="loadData"></param>
    /// <param name="complete"></param>
    /// <returns></returns>
    public static IEnumerator LoadStreamingAsset<T>(string fileName, LoadComplete<T> complete) where T : new()
    {
        string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
        using (UnityWebRequest request = UnityWebRequest.Get(dataPath))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
                complete(new T(), false);
            else // Success
            {
                complete(JsonUtility.FromJson<T>(request.downloadHandler.text), true);
            }
        }
#else
        try
        {
            complete(JsonUtility.FromJson<T>(LoadString(dataPath)), true);
        }
        catch (IOException)
        {
            complete(new T(), false);
        }
        yield return null;
#endif
    }

    /// <summary>
    /// Loads a serializable asset from the PersistentDataPath. The LoadComplete callback
    /// will be called when the object is ready to be used. All existing fields will be
    /// overwritten.
    /// 
    /// If the file cannot be found, the asset fields will retain their current values.
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="loadData"></param>
    /// <param name="complete"></param>
    /// <returns></returns>
    public static IEnumerator LoadPersistentData(string fileName, object loadData, LoadComplete complete)
    {
        string dataPath = Path.Combine(Application.persistentDataPath, fileName);
        
        try
        {
            JsonUtility.FromJsonOverwrite(LoadString(dataPath), loadData);
            complete(true);
        }
        catch (IOException)
        {
            JsonUtility.FromJsonOverwrite("", loadData);
            complete(false);
        }

        yield return null;
    }

    /// <summary>
    /// Loads a serializable asset from the PersistentDataPath into a new object. The LoadComplete
    /// callback will be called when the object is ready to be used.
    /// 
    /// If the file cannot be found, an empty asset will be returned.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fileName"></param>
    /// <param name="complete"></param>
    /// <returns></returns>
    public static IEnumerator LoadPersistentData<T>(string fileName, LoadComplete<T> complete) where T : new()
    {
        try
        {
            string dataPath = Path.Combine(Application.persistentDataPath, fileName);
            complete(LoadFileAbsolute<T>(dataPath), true);
        }
        catch (IOException)
        {
            complete(new T(), false);
        }
        yield return null;
    }

    /// <summary>
    /// Loads a serializable asset from an absolute path (does not append anything to the path).
    /// Use at your own risk, this is meant primarily for editor use.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="absoluteFilePath"></param>
    /// <returns></returns>
    public static T LoadFileAbsolute<T>(string absoluteFilePath) => JsonUtility.FromJson<T>(LoadString(absoluteFilePath));

    /// <summary>
    /// Saves a string to a specified file.
    /// </summary>
    /// <param name="toSave"></param>
    /// <param name="fileName"></param>
    private static void SaveString(string toSave, string dataPath)
    {
        // If the file is in an uncreated subdirectory, create the directory first.
        _ = Directory.CreateDirectory(Path.GetDirectoryName(dataPath));

        using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(toSave);
        }
    }

    /// <summary>
    /// Loads a Json script from a specified file.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns>Json string</returns>
    private static string LoadString(string dataPath)
    {
        string data;

        using (StreamReader streamReader = File.OpenText(dataPath))
        {
            data = streamReader.ReadToEnd();
        }
        return data;
    }
}
