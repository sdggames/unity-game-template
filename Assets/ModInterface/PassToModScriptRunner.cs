﻿using System;

/// <summary>
/// Attribute registers any static method as a standalone function for MoonSharp. Name can be changed.
/// </summary>
namespace MoonSharpRegistration
{
    [AttributeUsage(AttributeTargets.Method)]
    public class PassToModScriptRunner : Attribute
    {
        public string Name { get; set; }
        public PassToModScriptRunner(string command_name = null) => this.Name = command_name;
    }
}