﻿using Interface;

namespace Callouts
{
    public class Mod_HelloWorld
    {
        public static void SayHello() => _ = ModScriptRunner.Call("HelloWorld");
    }
}
