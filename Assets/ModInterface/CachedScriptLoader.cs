﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;

internal class CachedScriptLoader : ScriptLoaderBase
{
    public override object LoadFile(string file, Table globalContext)
    {
        return ModManager.LoadFile(file);
    }

    public override bool ScriptFileExists(string name)
    {
        return ModManager.ScriptFileExists(name);
    }
}
