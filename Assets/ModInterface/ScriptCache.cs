﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
using UnityEngine.Networking;
#endif

[Serializable]
public class ScriptCache
{
    /// <summary>
    /// Selects between the two operating modes a ScriptCache can use.
    /// The AiScriptCache will allow the user to turn each AI on or off individually.
    /// The ModScriptCache will allow the user to select between variants for each mod
    /// </summary>
    public enum ScriptCacheMode
    {
        AiScriptCache,
        ModScriptCache
    }

    /// <summary>
    /// The name of this folder in the file system.
    /// </summary>
    public string folderName;
    private ScriptCacheMode cacheMode;

    /// <summary>
    /// Create a new ScriptCache instance for the specified target directory.
    /// The AiScriptCache manages files from StreamingAssets/AiScripts
    /// The ModScriptCache manages files from StreamingAssets/ModScripts
    /// </summary>
    /// <param name="cacheMode"></param>
    public ScriptCache(ScriptCacheMode cacheMode)
    {
        this.cacheMode = cacheMode;
        switch (cacheMode)
        {
            case ScriptCacheMode.AiScriptCache:
                this.folderName = "AiScripts";
                break;
            case ScriptCacheMode.ModScriptCache:
                this.folderName = "ModScripts";
                break;
            default:
                throw new InvalidSwitchCaseException("ScriptCache type undefined");
        }
    }

    public FileManafest manafest = null;

    /// <summary>
    /// Local, cached copies of each loaded file that exist in RAM.
    /// </summary>
    public Dictionary<string, string> fileContents;

    /// <summary>
    /// Delegate will return when all file operations have completed.
    /// </summary>
    public delegate void LoadComplete();


    /// <summary>
    /// Coroutine to load each manafest file in the directory and subdirectory.
    /// Must be called before LoadScripts is called.
    /// </summary>
    /// <returns></returns>
    public IEnumerator BuildDirectory()
    {
        this.manafest = new FileManafest();

        IEnumerator loadManafests = this.manafest.LoadOrBuildManafestsRecursively(this.folderName);
        while (loadManafests.MoveNext()) yield return loadManafests.Current;
    }

    /// <summary>
    /// Loads all selected scripts into memory from disk or from over the network.
    /// </summary>
    /// <returns></returns>
    public IEnumerator LoadScripts()
    {
        Assert.IsNotNull(this.manafest, "Error: Attempted to load a script before the file manafest was generated!");

        this.fileContents = new Dictionary<string, string>();
        
        foreach (string fileName in this.GetFullFileNames(this.manafest, this.folderName, new List<string>()))
        {
            string dataPath = Path.Combine(Application.streamingAssetsPath, fileName);

#if !UNITY_EDITOR && (UNITY_WEBGL || UNITY_ANDROID)
            using (UnityWebRequest request = UnityWebRequest.Get(dataPath))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError)
                    throw new FileNotFoundException("File " + fileName + " was listed in manafest file, but doesn't actually exist on disk!");
                else
                    fileContents.Add(fileName.ToLower(), request.downloadHandler.text);
            }
#else
            using (StreamReader streamReader = File.OpenText(dataPath))
                this.fileContents.Add(fileName.ToLower(), streamReader.ReadToEnd());
#endif
        }

        // Generate a LoadScripts file dynamicly.
        if (this.cacheMode == ScriptCacheMode.ModScriptCache)
            this.GenerateModScriptsLoadScriptsFile();

        yield return null;
    }

    /// <summary>
    /// Returns an array containing the absolute paths to each of the directories 
    /// in the cache. (note: folders containing only other folders are not explicitly enumerated)
    /// </summary>
    /// <returns></returns>
    public string[] BuildPaths() => this.BuildPaths(this.manafest, this.folderName, new List<string>() { this.folderName }).ToArray();
    private List<string> BuildPaths(FileManafest currentDir, string currentPath, List<string> pathList)
    {
        // Add the current directory if it contains files.
        if (currentDir.files.Count > 0)
            pathList.Add(currentPath);

        // Call down into the next subdirectory.
        foreach (string subDir in currentDir.subDirectories)
            if (currentDir.subDirectoryContents[subDir].IsDirectoryActive)
                this.BuildPaths(currentDir.subDirectoryContents[subDir], currentPath + "/" + subDir, pathList);

        return pathList;
    }

    /// <summary>
    /// Looks for a file in the current mod folders and returns true if it exists.
    /// </summary>
    /// <param name="fullPath"></param>
    /// <returns></returns>
    public bool FileExists(string fullPath)
    {
        // Special case.
        if ((this.cacheMode == ScriptCacheMode.ModScriptCache) && fullPath.Equals(this.folderName + "/LoadScripts.lua", StringComparison.OrdinalIgnoreCase))
            return true;
        
        string[] fullDirectory = fullPath.Split('/');
        if (fullDirectory[0].Equals(this.folderName, StringComparison.OrdinalIgnoreCase))
            return this.FileExists(fullDirectory.Skip(1).ToArray(), this.manafest);
        return false;
    }
    private bool FileExists(string [] fullPath, FileManafest currentDirectory)
    {
        if (fullPath.Length == 1)
        {
            // We're looking for a folder.
            foreach (string file in currentDirectory.files)
                if (fullPath[0].Equals(file, StringComparison.OrdinalIgnoreCase))
                    return true;
        }
        else
        {
            // We're looking for a folder.
            foreach (string subdir in currentDirectory.subDirectories)
                if (fullPath[0].Equals(subdir, StringComparison.OrdinalIgnoreCase))
                {
                    FileManafest manafest = currentDirectory.subDirectoryContents[subdir];
                    if (manafest.IsDirectoryActive)
                        return this.FileExists(fullPath.Skip(1).ToArray(), manafest);
                }
       }
        // No valid match.
        return false;
    }

    /// <summary>
    /// Returns the contents of the requested file. Does not provide any error checking,
    /// call FileExists first to ensure that data will be returned.
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public object LoadFile(string filePath) => this.fileContents[filePath.ToLower()];

    /// <summary>
    /// Helper function to generate a list of all file names with full paths.
    /// </summary>
    /// <param name="currentDir"></param>
    /// <param name="currentPath"></param>
    /// <param name="fileList"></param>
    /// <returns>fileList (same object that was passed in, returned in case you want to assign it in a single line)</returns>
    private List<string> GetFullFileNames(FileManafest currentDir, string currentPath, List<string> fileList)
    {
        // Add the current directory if it contains files.
        foreach (string file in currentDir.files)
            fileList.Add(currentPath + "/" + file);

        // Call down into the next subdirectory.
        foreach (string subDir in currentDir.subDirectories)
            if (currentDir.subDirectoryContents[subDir].IsDirectoryActive)
                this.GetFullFileNames(currentDir.subDirectoryContents[subDir], currentPath + "/" + subDir, fileList);

        return fileList;
    }

    /// <summary>
    /// Helper function to generate a LoadScripts file automatically according to the ModScripts file rules.
    /// </summary>
    private void GenerateModScriptsLoadScriptsFile()
    {
        string loadScripts = "";

        foreach (string modName in this.manafest.subDirectories)
        {
            // Each mod needs two files: a FolderName.lua file and a FolderNameTests.lua file.
            loadScripts += "require '" + modName + "'\nrequire '" + modName + "Tests'\n";
        }

        this.fileContents.Add(this.folderName.ToLower() + "/loadscripts.lua", loadScripts);
    }
}
