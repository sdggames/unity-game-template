﻿using NUnit.Framework;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace SDGLibTests
{
    public class FileManagerTests
    {
        [Serializable]
        private class TestData : IEquatable<TestData>
        {
            public int specialNumber1;
            public int specialNumber2;

            public bool Equals(TestData other)
            {
                return other != null &&
                       this.specialNumber1 == other.specialNumber1 &&
                       this.specialNumber2 == other.specialNumber2;
            }

            public override string ToString()
            {
                return "[" + this.specialNumber1.ToString() + ", " + this.specialNumber2.ToString() + "]";
            }
        }

        private bool fileLoadFinished;
        private bool fileLoadSucceeded;
        private TestData fileLoadOut;

        [SetUp]
        public void SetUpFileTest()
        {
            this.fileLoadFinished = false;
            this.fileLoadSucceeded = false;
            this.fileLoadOut = null;
        }

        // Save streaming asset tests:
        [Test]
        public void StreamingAsset_SaveSuccess()
        {
            string fileName = "Temp/Test1.txt";
            string fullPath = Path.Combine(Application.streamingAssetsPath, fileName);

            TestData data = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            FileManager.SaveStreamingAsset(fileName, data);

            Assert.True(File.Exists(fullPath), "The file manager should have created a file at location " + fullPath + "!");
        }

        // Load Streaming asset tests
        public void LoadStreamingAsset_Direct_Success()
        {
            string fileName = "Temp/Test1.txt";

            TestData saveData = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };
            TestData loadData = new TestData();

            // We can only test the PC version here, but the mod integration will provide coverage for WebGl even if it isn't strictly via a unit test.
            FileManager.SaveStreamingAsset(fileName, saveData);
            FileManager.LoadStreamingAsset(fileName, loadData, this.LoadComplete).MoveNext();

            Assert.IsTrue(this.fileLoadSucceeded, "We should have received a thumbs-up, the file was loaded!");
            Assert.IsTrue(this.fileLoadFinished, "We never received a callback from the file manager!");
            Assert.AreEqual(saveData, loadData, "The data retrieved from the file manager does not match the data that was saved to disk!");
        }

        [Test]
        public void LoadStreamingAsset_Direct_FileNotFound()
        {
            string fileName = "Temp/Test2.txt";

            TestData loadData = new TestData()
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };
            TestData unmodified = new TestData()
            {
                specialNumber1 = loadData.specialNumber1,
                specialNumber2 = loadData.specialNumber2
            };

            FileManager.LoadStreamingAsset(fileName, loadData, this.LoadComplete).MoveNext();

            Assert.IsFalse(this.fileLoadSucceeded, "FileManager should barf gracefully when it encounters a missing file!");
            Assert.IsTrue(this.fileLoadFinished, "We should have received a callback from the file manager!");
            Assert.AreEqual(unmodified, loadData, "No data should be changed since the read failed. Any stale data should have been left as-is.");
        }

        // Load streaming asset <T> tests
        [Test]
        public void LoadStreamingAsset_Indirect_Success()
        {
            string fileName = "Temp/Test1.txt";

            TestData saveData = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            // We can only test the PC version here, but the mod integration will provide coverage for WebGl even if it isn't strictly via a unit test.
            FileManager.SaveStreamingAsset(fileName, saveData);
            FileManager.LoadStreamingAsset<TestData>(fileName, this.LoadComplete).MoveNext();

            Assert.IsTrue(this.fileLoadSucceeded, "We should have received a thumbs-up, the file was loaded!");
            Assert.IsTrue(this.fileLoadFinished, "We never received a callback from the file manager!");
            Assert.NotNull(this.fileLoadOut, "The File Manager didn't return a valid serializable object!");
            Assert.AreEqual(saveData, this.fileLoadOut, "The data retrieved from the file manager does not match the data that was saved to disk!");
        }

        [Test]
        public void LoadStreamingAsset_Indirect_FileNotFound()
        {
            string fileName = "Temp/Test2.txt";

            FileManager.LoadStreamingAsset<TestData>(fileName, this.LoadComplete).MoveNext();

            Assert.IsFalse(this.fileLoadSucceeded, "FileManager should barf gracefully when it encounters a missing file!");
            Assert.IsTrue(this.fileLoadFinished, "We should have received a callback from the file manager!");
            Assert.AreEqual(new TestData(), this.fileLoadOut, "No data should be present since the read failed. Any stale data should have been overwritten.");
        }

        // Save persistent data tests:
        [Test]
        public void SavePersistentData_Success()
        {
            string fileName = "Temp/Test1.txt";
            string fullPath = Path.Combine(Application.persistentDataPath, fileName);

            TestData data = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            FileManager.SavePersistentData(fileName, data);

            Assert.True(File.Exists(fullPath), "The file manager should have created a file at location " + fullPath + "!");
        }

        // Save file absolute tests:
        [Test]
        public void SaveFileAbsolute_Success()
        {
            string fileName = "Temp/Test1.txt";
            string fullPath = Path.Combine(Application.persistentDataPath, fileName);

            TestData data = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            FileManager.SaveFileAbsolute(fullPath, data);

            Assert.True(File.Exists(fullPath), "The file manager should have created a file at location " + fullPath + "!");
        }

        // Save file absolute tests:
        [Test]
        public void SaveFileAbsolute_Fail()
        {
            string fileName = "Temp/Test1.txt";
            string fullPath = Path.Combine(Application.persistentDataPath, fileName);

            TestData data = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            FileManager.SaveFileAbsolute(fileName, data);

            Assert.False(File.Exists(fullPath), "The file manager should not have created a file at location " + fullPath + "!");
        }

        // Load Streaming asset tests
        [Test]
        public void LoadPersistentData_Direct_Success()
        {
            string fileName = "Temp/Test1.txt";

            TestData saveData = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };
            TestData loadData = new TestData()
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            // We can only test the PC version here, but the mod integration will provide coverage for WebGl even if it isn't strictly via a unit test.
            FileManager.SavePersistentData(fileName, saveData);
            FileManager.LoadPersistentData(fileName, loadData, this.LoadComplete).MoveNext();

            Assert.IsTrue(this.fileLoadSucceeded, "We should have received a thumbs-up, the file was loaded!");
            Assert.IsTrue(this.fileLoadFinished, "We never received a callback from the file manager!");
            Assert.AreEqual(saveData, loadData, "The data retrieved from the file manager does not match the data that was saved to disk!");
        }

        [Test]
        public void LoadPersistentData_Direct_FileNotFound()
        {
            string fileName = "Temp/Test2.txt";

            TestData loadData = new TestData()
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };
            TestData unmodified = new TestData()
            {
                specialNumber1 = loadData.specialNumber1,
                specialNumber2 = loadData.specialNumber2
            };

            FileManager.LoadPersistentData(fileName, loadData, this.LoadComplete).MoveNext();

            Assert.IsFalse(this.fileLoadSucceeded, "FileManager should barf gracefully when it encounters a missing file!");
            Assert.IsTrue(this.fileLoadFinished, "We should have received a callback from the file manager!");
            Assert.AreEqual(unmodified, loadData, "No data should be changed since the read failed. Any stale data should have been left as-is.");
        }

        // Load persistent data <T> tests
        [Test]
        public void LoadPersistentData_Indirect_Success()
        {
            string fileName = "Temp/Test1.txt";

            TestData saveData = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            // We can only test the PC version here, but the mod integration will provide coverage for WebGl even if it isn't strictly via a unit test.
            FileManager.SavePersistentData(fileName, saveData);
            FileManager.LoadPersistentData<TestData>(fileName, this.LoadComplete).MoveNext();

            Assert.IsTrue(this.fileLoadSucceeded, "We should have received a thumbs-up, the file was loaded!");
            Assert.IsTrue(this.fileLoadFinished, "We never received a callback from the file manager!");
            Assert.NotNull(this.fileLoadOut, "The File Manager didn't return a valid serializable object!");
            Assert.AreEqual(saveData, this.fileLoadOut, "The data retrieved from the file manager does not match the data that was saved to disk!");
        }

        [Test]
        public void LoadPersistentData_Indirect_FileNotFound()
        {
            string fileName = "Temp/Test2.txt";

            FileManager.LoadPersistentData<TestData>(fileName, this.LoadComplete).MoveNext();

            Assert.IsFalse(this.fileLoadSucceeded, "FileManager should barf gracefully when it encounters a missing file!");
            Assert.IsTrue(this.fileLoadFinished, "We should have received a callback from the file manager!");
            Assert.AreEqual(new TestData(), this.fileLoadOut, "No data should be present since the read failed. Any stale data should have been overwritten.");
        }

        // Load file absolute tests
        [Test]
        public void LoadFileAbsolute_Success()
        {
            string fileName = "Temp/Test1.txt";
            string fullPath = Path.Combine(Application.persistentDataPath, fileName);

            TestData saveData = new TestData
            {
                specialNumber1 = UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                specialNumber2 = UnityEngine.Random.Range(int.MinValue, int.MaxValue)
            };

            // We can only test the PC version here, but the mod integration will provide coverage for WebGl even if it isn't strictly via a unit test.
            FileManager.SavePersistentData(fileName, saveData);
            TestData loadedData = FileManager.LoadFileAbsolute<TestData>(fullPath);

            Assert.NotNull(loadedData, "The File Manager didn't return a valid serializable object!");
            Assert.AreEqual(saveData, loadedData, "The data retrieved from the file manager does not match the data that was saved to disk!");
        }

        [Test]
        public void FileAbsolute_FileNotFound()
        {
            string fileName = "Temp2/Test2.txt";
            string fullPath = Path.Combine(Application.persistentDataPath, fileName);

            try
            {
                FileManager.LoadFileAbsolute<TestData>(fullPath);
                Assert.Fail("We should not have loaded anything!");
            }
            catch (DirectoryNotFoundException)
            {
                // We want this.
            }
        }

        private void LoadComplete(bool success)
        {
            this.fileLoadSucceeded = success;
            this.fileLoadFinished = true;
        }

        private void LoadComplete(TestData data, bool success)
        {
            this.fileLoadSucceeded = success;
            this.fileLoadFinished = true;
            this.fileLoadOut = data;
        }

        [TearDown]
        public void RemoveTemporaryFiles()
        {
            FileUtil.DeleteFileOrDirectory(Path.Combine(Application.streamingAssetsPath, "Temp"));
            FileUtil.DeleteFileOrDirectory(Path.Combine(Application.persistentDataPath, "Temp"));
        }
    }
}
