﻿using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    internal class ManagedTest : SeededTest
    {
        internal Test_SceneManager Manager { get; set; }

        [SetUp]
        public void CreateTestManager() => this.Manager = new Test_SceneManager();

        [TearDown]
        public void RemoveTestManager() => this.Manager.Destroy();

    }
}
