﻿using UnityEngine;

namespace Tests
{
    internal class Test_SceneManager
    {
        internal Test_SceneManager()
        {
            this.Manager = new GameObject("Game Manager Scripts");

            // Create one gamemanager object with every manager type we test. Call start and awake on each manager as needed.
        }

        // Create a gameobject to house all manager scripts.
        private GameObject Manager { get; set; }

        /// <summary>
        /// Destroys the manager gameobject and clears all pointers so singletons can be reused.
        /// </summary>
        internal void Destroy()
        {
            Object.DestroyImmediate(this.Manager);
        }

        /// <summary>
        /// Callout to set the maze heights through a lua script.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SetMazeHeights(Vector2Int mazeSize)
        {
        }

        ~Test_SceneManager()
        {
            if (this.Manager != null)
            {
                this.Destroy();
            }
        }
    }
}
